import 'dart:convert';
import 'package:fics_chess/checkerboard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'fics_client.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (_) => FicsClient(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Lichess Flutter Client',
        initialRoute: '/',
        routes: {
          '/': (context) => MainPage(),
        },
        theme: ThemeData(
          primaryColor: Colors.lightGreen,
        ),
      ),
    ),
  );
}

class MainPage extends StatefulWidget {
  @override
  MainPageState createState() {
    return MainPageState();
  }
}

enum MainActions { listGames }

class MainPageState extends State<MainPage> {
  final ScrollController _scrollController = ScrollController();
  bool _scrollNeeded = false;

  var inputField = TextEditingController();
  late FocusNode inputFieldFocusNode;

  @override
  void initState() {
    super.initState();
    inputFieldFocusNode = FocusNode();
  }

  @override
  void dispose() {
    inputFieldFocusNode.dispose();
    super.dispose();
  }

  _scrollToEnd() async {
    if (_scrollNeeded) {
      _scrollNeeded = false;
      _scrollController.animateTo(_scrollController.position.maxScrollExtent,
          duration: Duration(milliseconds: 200), curve: Curves.easeInOut);
    }
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance!.addPostFrameCallback((_) => _scrollToEnd());
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Scaffold(
        appBar: AppBar(
          bottom: const TabBar(
            tabs: [
              Tab(
                text: 'Console',
              ),
              Tab(
                text: 'Seekers',
              )
            ],
          ),
          title: Consumer<FicsClient>(
            builder: (context, client, child) => Text(
              'FICS Client - ${client.isConnected ? 'connected' : 'not connected'}',
            ),
          ),
          actions: <Widget>[
            IconButton(
              icon: Provider.of<FicsClient>(context, listen: true).isConnected
                  ? const Icon(Icons.logout)
                  : const Icon(Icons.login),
              tooltip:
                  Provider.of<FicsClient>(context, listen: true).isConnected
                      ? 'Disconnect'
                      : 'Connect',
              onPressed: () async {
                if (Provider.of<FicsClient>(context, listen: false)
                    .isConnected) {
                  Provider.of<FicsClient>(context, listen: false)
                      .send('logout');
                } else {
                  await Provider.of<FicsClient>(context, listen: false)
                      .connect();
                }
              },
            ),
            PopupMenuButton<MainActions>(
              onSelected: (MainActions result) {
                // Doing nothing for now
              },
              itemBuilder: (BuildContext context) =>
                  <PopupMenuEntry<MainActions>>[
                const PopupMenuItem<MainActions>(
                  value: MainActions.listGames,
                  child: Text('List Games'),
                ),
              ],
            ),
          ],
        ),
        body: TabBarView(children: [
          Column(
            children: [
              Expanded(
                child: ListView.builder(
                    controller: _scrollController,
                    itemCount: Provider.of<FicsClient>(context, listen: true)
                        .consoleText
                        .length,
                    itemBuilder: (BuildContext context, int index) {
                      _scrollNeeded = true;
                      var bufferItem =
                          Provider.of<FicsClient>(context, listen: false)
                              .consoleText[index];
                      return Text(bufferItem.data,
                          key: ValueKey(bufferItem.key),
                          softWrap: false,
                          style: TextStyle(fontFamily: "Courier New"));
                    }),
              ),
              TextField(
                focusNode: inputFieldFocusNode,
                autofocus: true,
                controller: inputField,
                decoration: InputDecoration(
                  labelText: 'FICS% ',
                  border: OutlineInputBorder(),
                ),
                onSubmitted: (data) {
                  print(data);
                  Provider.of<FicsClient>(context, listen: false).send(data);
                  inputField.text = '';
                  inputFieldFocusNode.requestFocus();
                },
              ),
            ],
          ),
            Center(
              child: Text(
                'Seekers'
              ),
            ),
        ]),
      ),
    );
  }
}
