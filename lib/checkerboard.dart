import 'package:flutter/material.dart';

class CheckerBoard extends StatefulWidget {
  @override
  CheckerBoardState createState() {
    return CheckerBoardState();
  }
}

class CheckerBoardState extends State<CheckerBoard> {
  final List<Widget> squares = [];

  @override
  void initState() {
    super.initState();
    for (var x = 0; x < 8; x++) {
      for (var y = 0; y < 8; y++) {
        squares.add(new Square(x: x, y: y));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.black,
            width: 7,
          ),
        ),
        width: 180,
        height: 180,
        child: GridView.count(
            padding: const EdgeInsets.all(0),
            crossAxisCount: 8,
            children: squares));
  }
}

class Square extends StatelessWidget {
  final int x;
  final int y;

  Color? getColor() {
    if (x % 2 == y % 2) {
      return Colors.lightGreen[800];
    }
    return Colors.amber[100];
  }

  const Square({Key? key, required this.x, required this.y}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: getColor(),
    );
  }
}
