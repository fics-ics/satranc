import 'dart:io';
import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';

class FicsClient extends ChangeNotifier {
  static final FicsClient _instance = FicsClient._internal();
  factory FicsClient() => _instance;

  Socket? socket;
  int lineNumber = 0;
  List<FicsBufferItem> consoleText = [];
  bool isConnected = false;

  FicsClient._internal();

  Future<bool> connect() async {
    socket = await Socket.connect('freechess.org', 5000);
    if (socket != null) {
      socket!.listen(_dataHandler, onError: _errorHandler, onDone: _doneHandler);
      this.isConnected = true;
      return true;
    }
    else {
      this.isConnected = false;
      return false;
    }
  }

  send(String data) {
    if (socket != null)
      {
        socket!.writeln(data);
      }
  }

  void _dataHandler(data) {
    String d = String.fromCharCodes(data);
    this.consoleText.add(FicsBufferItem(lineNumber, d));
    notifyListeners();
  }

  void _doneHandler() {
    if (socket != null) {
      socket!.destroy();
      socket = null;
      this.isConnected = false;
    }
    notifyListeners();
  }

  void _errorHandler(error, StackTrace trace) {
    print(error);
  }
}

class FicsBufferItem {
  int key;
  String data;

  FicsBufferItem(this.key, this.data);
}


class FicsParser {

}
